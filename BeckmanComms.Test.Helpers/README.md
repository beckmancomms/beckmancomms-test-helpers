﻿# Beckman Institute Test Helpers

Contains frequently used base classes and snippets for unit tests.

Available as a NuGet package from the Beckman internal NuGet gallery (`G:\NuGetFeed`).