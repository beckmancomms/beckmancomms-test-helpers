﻿using Xunit.Abstractions;

namespace BeckmanComms.Test.Helpers.Xunit.Base
{
    /// <summary>
    /// Adds output utilities to XUnit tests.
    /// </summary>
    public abstract class LoggableTest
    {
        /// <summary>
        /// Test output helper.
        /// </summary>
        protected ITestOutputHelper Output;

        /// <summary>
        /// LoggableTest constructor.
        /// </summary>
        /// <param name="output">Test output helper.</param>
        protected LoggableTest(ITestOutputHelper output)
        {
            Output = output;
        }
    }
}